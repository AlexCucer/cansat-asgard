/** @file 
 * @brief Replacement file for the standard assert.h.
 * 
 * This header puts an info message if the standard assert.h
 * is included with symbol __ASSERT_USE_STDERR defined: 
 * It causes an extra 122 bytes of dynamic memory to be used
 * (on Arduino Uno), to store the various information passed 
 * to function __assert(). 
 */

 #pragma once
 #ifdef __ASSERT_USE_STDERR
 #error "Including assert.h with symbol __ASSERT_USE_STDERR defined consumes about 120 bytes dynamic memory. Include AssertCSPU.h instead and use DASSERT macroes."
 #endif
