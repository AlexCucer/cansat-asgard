/*
   Timer.h

   A small utility class to easily time method execution.
   Usage:
      * define symbol USE_TIMER
      * Either: 
          Just declare a Timer variable as local variable in the block to be timed. 
          The constructor starts a timer and the destructor reads and outputs it on the Serial interface.
        OR (recommanded):
          Use it through the DBG_TIMER macro to preserve a 0 memory-footprint in the operational software.
      * Embedded timers are supported (and nicely displayed using indentation).
*/

#pragma once


#include "elapsedMillis.h"

class Timer {
  public:
    Timer(__FlashStringHelper* msg);
    ~Timer();

  protected:
    static byte level; // The embedding level.
    elapsedMillis internalTimer;
    __FlashStringHelper* location;
};

#ifdef USE_TIMER
#   define DBG_TIMER(blockName)  Timer _dbg_Timer((__FlashStringHelper*) F(blockName)); 
    // NB: __FUNCTION__ is gcc-specific 
#else
#  define DBG_TIMER(blockName)
#endif
