/* 
 *  Functions used for debugging. Do not make conditional to DEBUG:
 *  Library is compiled into a .a and actually linked if used only. 
 *  Keep this file minimal to avoid memory usage when debugging is off. 
 */

#include "DebugCSPU.h"
#include "Arduino.h"

/* Return the number of free bytes.
 * This function has a ZERO memory foot-print. 
 */
int freeRam () {
#ifndef USING_SIMULATED_ARDUINO
    extern int __heap_start, *__brkval;
    int v;
    return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
#else
    return 0;
#endif
}


