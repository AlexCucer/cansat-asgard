/* 
   DebugCSPU.h
  */
 /** @file
    @brief Main header for the Common debugging macros and utilities library.

    DebugCTIV.h includes all other header files, and should be the only one explicitely included.
    Be sure to (un)define USE_ASSERTION and DEBUG to include the actual features in your test executable
    (and be sure not to include them in your operation executables).
    
    Typical use: 
    @code
    #define USE_ASSERTION // Comment this line out for operational software
    #define DEBUG
    #include "DebugCSPU.h"
    #define DBG_xxxx 0
    #define DBG_yyyy 1
    @endcode


   

 *******************************************************************************************/
 /** @file */ 
 /** @defgroup DebugCSPU DebugCSPU library 
 *  @brief  Common debugging macros and utilities any Arduino project.
 *  
 *  The DebugCSPU library contains various objects, macro-definitions etc to support debugging with a zero 
 *  memory foot-print in the operational software.
 *  It also provides the NULL definition (if not provided anywhere else), since std::nullptr is not 
 *  compatible with Arduino v1.6 (see #NULL) and the NULL definition is in stddef.h, which has a significant
 *  memory footprint. 
 *  
 *  ADD HERE INFO FROM SW design document, document typical use.
 *  - ....
 *  
 *  @remarks 
 *  We could not find any way to detect whether Serial.begin() has previously
 *  been called or not. This library curently assumes the Serial port has been initialized,
 *  and defines macro DINIT() even when DEBUG is not defined.
 *  
 *  _Dependencies_\n
 *  None.
 *  
 *  
 *  _History_\n
 *  The library was created by the 2017-18 Cansat team (ISATIS)
 *  
 *  @{
 */

#pragma once
#include "Arduino.h"
#include "AssertCSPU.h"
#include "SerialStream.h"

/** This is required to avoid including stddef.h for the NULL definition.
    Using std::nullptr is not an option because in Arduino v1.6, the required gcc option is 
    not activated to make nullptr available in namespace std. */
#ifndef NULL 
/** @ingroup DebugCSPU.h test */
#define NULL (void *) 0
#endif


// Call DINIT once in setup() if the serial port to the computer is used in any library
// or in any part of the sketch..
// Macros do not call functions in order to reduce memory usage.
#define DINIT(baudRate)  Serial.begin(baudRate); while (!Serial); Serial.println(F("Serial link OK"));

#ifdef DEBUG
// -------- Define tracing macros -----

// Actual macros.
// Print a debugging STRING msg, if the moduleTag != 0 (no final carriage return)
// The string is not stored in dynamic memory
#define DPRINTS1(moduleTag, msg)   if(moduleTag!=0) Serial.print(F(msg));
#define DPRINTS2(moduleTag, msg,format)   if(moduleTag!=0) Serial.print(F(msg),format);

// Print a debugging STRING msg, if the moduleTag != 0 (with final carriage return)
// The string is not stored in dynamic memory
#define DPRINTSLN1(moduleTag, msg)  if(moduleTag!=0) Serial.println(F(msg));
#define DPRINTSLN2(moduleTag, msg,format)  if(moduleTag!=0) Serial.println(F(msg),format);

// Print a debugging NUMERIC value, if the moduleTag != 0 (no final carriage return)
#define DPRINT1(moduleTag, numValue)    if(moduleTag!=0) Serial.print(numValue);
#define DPRINT2(moduleTag, numValue,format)    if(moduleTag!=0) Serial.print(numValue,format);

// Print a debugging NUMERIC value, if the moduleTag != 0 (with final carriage return)
#define DPRINTLN1(moduleTag, numValue)  if(moduleTag!=0) Serial.println(numValue);
#define DPRINTLN2(moduleTag, numValue,format)  if(moduleTag!=0) Serial.println(numValue,format);

// Wait for x msec if the moduleTag != 0  is active.
#define DDELAY(moduleTag,durationInMsec)    if(moduleTag!=0) delay(durationInMsec);

// Print free ram if the moduleTag != 0
#define DFREE_RAM(moduleTag) if(moduleTag!=0)  { Serial.print(F("Free RAM: ")); Serial.print(freeRam()); Serial.println(F(" bytes."));}

// Allow for variable number of arguments (2 or 3)  (a bit tricky, but fool-proof).
// Variable number of arguments for printing string is not supported by Serial.print() but provided
// for consistency.
// Ref: https://stackoverflow.com/questions/11761703/overloading-macro-on-number-of-arguments
#define DGET_MACRO(moduleTag, _1,_2,NAME,...) NAME

#define DPRINT(moduleTag, ...) DGET_MACRO(moduleTag, __VA_ARGS__, DPRINT2, DPRINT1)(moduleTag,__VA_ARGS__)
#define DPRINTLN(moduleTag, ...) DGET_MACRO(moduleTag, __VA_ARGS__, DPRINTLN2, DPRINTLN1)(moduleTag,__VA_ARGS__)
#define DPRINTS(moduleTag, ...) DGET_MACRO(moduleTag, __VA_ARGS__, DPRINTS2, DPRINTS1)(moduleTag,__VA_ARGS__)
#define DPRINTSLN(moduleTag, ...) DGET_MACRO(moduleTag, __VA_ARGS__, DPRINTSLN2, DPRINTSLN1)(moduleTag,__VA_ARGS__)

// -------- Function prototypes
int freeRam ();  // Return free dynamic memory in bytes.
#else
// Define all macros but DINIT() as blank lines.
#define DPRINTS(moduleTag, msg,...)
#define DPRINTSLN(moduleTag, msg,...)
#define DPRINT(moduleTag, numValue,...)
#define DPRINTLN(moduleTag, numValue,...)
#define DDELAY(moduleTag,durationInMsec,...)
#define DFREE_RAM(moduleTag)
#endif

/** @} */
