/*
 * Functions provided by source 2.
 */
#pragma once 
#include "test_lib_config.h"
/* 
 * Just blink the built-in LED 3 times and send a debug message on the serial line.
 */

int dummyLibFunctionFromSrc2();
