/*
 * xxxxInterface.h
 * This file contains definitions shared by the CanSat, and the ground systems
 */

/**  @ingroup xxxxCSPU
 *  Constants used to manage the type of CSV strings exchanged between the CanSat and 
 *  the ground systems.
 */
 enum RecordType { 
                DataRecord=0, /**< Value denoting a measurement record */
                CmdRequest=1, /**< Value denoting a command (request)  */
                CmdResponse=2 /**< Value denoting a command response   */
 }; 

 /**  @ingroup xxxxCSPU
  *   Constants used to identify command requests */
 enum CommandRequestType {
                InitiateCmdMode=0,
                TerminateCmdMode=1,
                // To complete
 };

 /**  @ingroup xxxxCSPU
  *   Constants used to identify command responses */
 enum CommandResponseType {
                CmdModeInitiated=0,
                CmdModeTerminated=1,
                // To complete
 };


