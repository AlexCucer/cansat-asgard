/*
 * xxxxCSPU.h 
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup xxxxCSPU 
 * in the class documentation block.
 */

 /** @defgroup xxxxCSPU xxxxCSPU library 
 *  @brief The library of classes specific to the CanSat 2019 xxxx project.
 *  
 *  The xxxx library contains all the code for the xxxx project, which is assumed not to be reused accross project
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - DebugCSPU
 *  - TimeCSPU
 *  - elapsedMillis
 *  - cansatAsgardCSPU
 *  @todo TO BE COMPLETED.
 *  
 *  
 *  _History_\n
 *  The library was created by the 2018-2019 Cansat team (xxxxx) based on the IsatisCSPU library.
 */

