/*
   Test for class SD_Logger.

   It supposes the SD_Reader is available.
   Test 1 should succeed when no card is inserted, others should succeed
   when a card is inserted.

   ENCORE A FAIRE:
   card full
*/

const byte CS_Pin = 4;
//#define FILL_THE_CARD

#define DEBUG
#define USE_ASSERTIONS
#include "DebugCSPU.h"

#include "elapsedMillis.h"
#include "SD_Logger.h"

bool cardOK = false;

unsigned long fileSize(const String& fileName);


void test1_NoInit() {
  SD_Logger SD;

  Serial << F("Testing absent SD module...") << ENDL;
  
  char result = SD.init("abcd", CS_Pin + 1);
  // Should fail (wrong CS_Pin)
  DASSERT(result < 0);
  Serial << F("OK") << ENDL;

  Serial << F("Testing with an SD Module...") << ENDL;
  result = SD.init("abcd", CS_Pin);
  if (result < 0) {
    Serial << F("Test with no card OK. Others skipped") << ENDL;
    cardOK = false;
  }
  else if (result == 1) {
    Serial << F("Not enough space on card.") << ENDL;
    cardOK = false;
  }
  else {
    Serial << F("Test with no card skipped. Performing other tests") << ENDL;
    cardOK = true;
  }
}

void test2_VariousInitConditions()
{
  SD_Logger SD;

  Serial << F("2. Init conditions") << ENDL;
  Serial << F("Requesting more space than available") << ENDL;
  char result = SD.init("abcd", CS_Pin, "", 10000 );
  // Should fail (not enough space)
  DASSERT(result == 1);
  Serial << F("OK") << ENDL;
 
  // Test init without empty message
  Serial << F("Init with empty msg") << ENDL;
  result = SD.init("abcd", CS_Pin, "");
  DASSERT(result == 0);
  DASSERT(SD.fileSize() == 0);
  
  // Check structre of fileName()
  String name = SD.fileName();
  DASSERT(name.startsWith("abcd"));
  DASSERT(name.endsWith(".txt"));
  String numStr = name.substring(4, 8);
  DASSERT(numStr.charAt(0) == '0');
  int num = numStr.toInt();
  DASSERT(num > 0);
  DASSERT(num < 1000);
  Serial << F("OK") << ENDL;

  // Test init with non empty message.
  Serial << F("Init with non-empty msg") << ENDL;
  String str = "012345678901234567890123456789012345";
  result = SD.init("abcd", CS_Pin, str);
  DASSERT(result == 0);
  Serial << F("str.length()=") << str.length() << F(", file size=") << SD.fileSize() << ENDL;
  DASSERT(SD.fileSize() == (str.length() + 2)); // End of line is 2 characters.
  Serial << F("OK") << ENDL;
}

void test3_logging()
{
  SD_Logger SD;

  Serial << F("3. Logging to file") << ENDL;
  char result = SD.init("abcd", CS_Pin, "", 100 );
  DASSERT(result == 0);
  unsigned long size = SD.fileSize();
  String str = F("first line");
  size += (str.length() + 2);
  bool bresult = SD.log(str);
  DASSERT(bresult);
  DASSERT(SD.fileSize() == size);
  str = F("second line");
  size += (str.length() + 2);
  bresult = SD.log(str);
  DASSERT(bresult);
  DASSERT(SD.fileSize() == size);

  str = F("third line");
  size += (str.length() + 2);
  bresult = SD.log(str);
  DASSERT(bresult);
  DASSERT(SD.fileSize() == size);


  str = F("fourth line, is much longer than the previous ones. This is to test how large strings are managed by the library");
  str += F(" and check for possible overflow. Since we will not log more than about 50 characters at a time, this should be ");
  str += F(" more than we need. \n(There is a \\n character just before this parenthesis).");
  size += (str.length() + 2);
  bresult = SD.log(str);
  DASSERT(bresult);
  DASSERT(SD.fileSize() == size);

  Serial << F("OK") << ENDL;
}

void test4_performance() {
  SD_Logger SD;
  const byte numWrites = 100;
  bool bresult;

  Serial << F("5. Checking performance: writing ") << numWrites << F(" x 60 bytes to file") << ENDL;
  char result = SD.init("abcd", CS_Pin, "", 100 );
  DASSERT(result == 0);
  String str = F("012345678901234567890123456789012345678901234567890123456789");
  elapsedMillis fromStart = 0;
  for (int i = 0 ; i < numWrites; i++) {
    bresult = SD.log(str);
    DASSERT(bresult);
  }
  long duration = fromStart;
  long oneWriteDuration = duration / numWrites;
  duration /= 1000;
  Serial << F("Elapsed time = ") << duration << F(" seconds.") << ENDL;
  Serial << F("This is an average ") << oneWriteDuration << F(" msec / write") << ENDL;

  Serial << ENDL << ENDL << F("triggering maintenance (.=500 ms), should happen every 10 sec.") << ENDL;

  while (!SD.doIdle()) {
    delay(500);
    Serial << ".";
    Serial.flush();
  }

  Serial << F("Writing again...") << ENDL;
  str = F("");
  fromStart = 0;
  char buf[8];
  for (int i = 0 ; i < numWrites; i++) {
    str += itoa(i, buf, 10);
    bresult = SD.log(str);
    DASSERT(bresult);
  }
  duration = fromStart;
  oneWriteDuration = duration / numWrites;
  duration /= 1000;
  Serial << F("Elapsed time = ") << duration << F(" seconds.") << ENDL;
  Serial << F("This is an average ") << oneWriteDuration << F(" msec / write") << ENDL;
  Serial << F("*** Check content of file '") << SD.fileName() << F("'. It should contain all 99 first integers in the last line.") << ENDL;
}

#ifdef FILL_THE_CARD
void test_fillingUpTheCard()
{
  SD_Logger logger;
  const char* bigFileName = "bigfile.txt";

  // Copy file until free space < 1000 bytes.
  DASSERT(SD.begin(chipSelectPinNumber));
  if (!SD.exists(bigFileName)) {
    Serial << "Error: please provide 1Mb file named " << bigFileName << ENDL;
  }
  File dataFile = SD.open(bigFileName, FILE_READ);
  DASSERT(dataFile);
  unsigned long fileSize = dataFile.fileSize();
  dataFile.close();
  char result = logger.init("abcd", CS_Pin, "", 100 );
  DASSERT(result == 0);
  while (logger.freeSpaceInBytes() > fileSize) {

  }

  // Log more than 1000 bytes.
  Serial << F("Checking how card full is handled...") << ENDL;


  // Create a 2Megs file
  unsigned long
}
#endif

//-----------------------------------------------------------------------------------------------------------------------

void setup() {
  DINIT(9600);
  Serial << F("Testing SD_Logger...") << ENDL;

#ifdef CLOSE_AFTER_EACH_ACCESS
  Serial << F("NB: SD_Logger closes the file after each access.") << ENDL;
#else
  Serial << F("NB: SD_Logger does NOT close the file after each access.") << ENDL;
#endif
#ifdef STORE_SDFAT_OBJECT
  Serial << F("NB: SD_Logger stores the SdFat object. Logger size: ") << sizeof(SD_Logger) << F(" bytes")<< ENDL;
#else
  Serial << F("NB: SD_Logger does NOT store the SdFat object. Logger size: ") << sizeof(SD_Logger) << F(" bytes") << ENDL;
#endif
  test1_NoInit();
  if (cardOK) {
    //Run other tests.
#ifdef FILL_THE_CARD
    test_fillingUpTheCard();
#else
    test2_VariousInitConditions();
    test3_logging();
    test4_performance();
#endif
  }
  Serial << F("End of tests.") << ENDL;

}

void loop() {
  // put your main code here, to run repeatedly:

}
