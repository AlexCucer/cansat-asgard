/*
    StorageManager.h
    A class implementing all storage functions common to any Arduino project.

    Created on: 19 janv. 2018

*/

#pragma once
#include "HardwareScanner.h"
#include "SD_Logger.h"
#include "EEPROM_BankWriter.h"

/** @ingroup cansatAsgardCSPU
 *  @brief A COMPLETER
 *  
 *  DESCRIPTION A COMPLETER.
 */
class StorageManager {
  public:
    StorageManager(unsigned int theRecordSize, unsigned int theCampainDurationInSec, unsigned int theMinStoragePeriodInMsec);
    NON_VIRTUAL ~StorageManager() {};

    /* Prepare the StorageManager for work. Call once (in setup()) before calling storeOneRecord().
       If implemented in a subclass, be sure to call inherited::init() before performing additional initialization.
       Returns true if the init was successful, false otherwise. This init is considered successful if at least one
       storage destination was successfully initiailized.
    */
    NON_VIRTUAL bool init(HardwareScanner* hwScanner = NULL,
                      const char * fourCharPrefix = "abcd",
                      const byte chipSelectPinNumber = 4,
                      const unsigned int requiredFreeMegs = 100,
                      const EEPROM_BankWriter::EEPROM_Key key = 0x1234,
                      const String& loggerFirstLine="");

    /* Store a record of data. The size is expected to match the size defined when calling init()
       binaryData is just recordSize bytes of information, to be stored as-is in memory.
       stringData is a readable version of the same information, larger but humain-readable, to store on the SD card, where
       storage is cheap and unlimited.  */
    NON_VIRTUAL void storeOneRecord(const byte* binaryData, const String& stringData, const bool useEEPROM=true);

    /* Get number of records that can be stored in EEPROM */
    NON_VIRTUAL unsigned long getNumFreeEEEPROM_Records() const; 
    NON_VIRTUAL bool SD_Operational() const;
    
    /* Call this method regularly to allow for internal maintenance */ 
    NON_VIRTUAL void doIdle();

  private:
    /*Actually perform the storage of the record. */

    unsigned int recordSize;
    unsigned int campainDuration;   // The expected duration of the measurement campain, in seconds.
    unsigned int minStoragePeriod;  // The minimum delay between 2 storage requests in msec.
    bool initSD;                    // True if SD card was succesfully initialized.
    bool initEEPROM;                // Ture if EEPROM_Bank was successfully initialized. 
   
    EEPROM_BankWriter eeprom;
    SD_Logger logger;
};

