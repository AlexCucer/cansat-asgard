
/*
 * This utility program writes at most NUM_RECORDS records in the EEPROMs.
 * The size of the records increases from MinRecordSize to MaxRecordSize.
 * The content of the records is increasing numbers.
 * If REREAD_RECORDS is defined, it reads the records back to check their value.
 *
 * This program is used to check no boundary conditions causes failure during either the
 * write or the read.
 *
 */

#include "HardwareScanner.h"
#include "EEPROM_Bank.h"
#include "EEPROM_BankWithTools.h"
#include "SerialStream.h"

//#define DETAILED_OUTPUT
//#define TERSE_OUTPUT   // Define to deactivate most output.
#define REREAD_RECORDS  // Define to read records after writing.
const byte MinRecordSize=1;
const byte MaxRecordSize=255;
const unsigned long NUM_RECORDS = 100000;

EEPROM_Bank::EEPROM_Key testKey=0x6678;
int errorCount=0;

byte  buffer[MaxRecordSize];
byte referenceRecord[MaxRecordSize];
HardwareScanner hw;
EEPROM_BankWriter::EEPROM_Header previousHeader;

void printHeader(EEPROM_BankWriter::EEPROM_Header h) {
	Serial << "firstFreeChip=" << h.firstFreeChip << ", firstFreeByte=" <<h.firstFreeByte << ENDL;
}

void checkEEPROM_Header(EEPROM_BankWriter::EEPROM_Header h, byte recSize)
{
	//printHeader(previousHeader);
	//printHeader(h);
	if (previousHeader.firstFreeChip != 99) {
		// Actually check
		if (h.firstFreeChip == previousHeader.firstFreeChip) {
			if ((h.firstFreeByte - previousHeader.firstFreeByte) != recSize) {
				Serial << F("Error in evolution of firstFreeByte: ") << ENDL;
#ifdef DETAILED_OUTPUT
				printHeader(previousHeader);
				printHeader(h);
#endif
			}
		} else {
#ifdef DETAILED_OUTPUT
			Serial << "Entered next chip" << ENDL;
			printHeader(previousHeader);
			printHeader(h);
#endif
		}
	}
	previousHeader.firstFreeByte=h.firstFreeByte;
	previousHeader.firstFreeChip=h.firstFreeChip;
}

void hexDump(byte *data, byte size)
{
  char str[10];
  for (int i = 0; i < size; i++) {
    if ((i % 16) == 0) {
      if (i != 0) Serial << ENDL;
      sprintf(str, "  %04x:", i);
      Serial << str;
    }
    if ((i % 4) == 0) Serial << "  ";
    sprintf(str, "%02x ", data[i]);
    Serial << str;
  }
  Serial << ENDL;
}


void initReferenceRecord() {
	for (int i = 0; i< MaxRecordSize;i++)
		referenceRecord[i]=i;
}

void testOneRecordSize(const byte size)
{
  EEPROM_BankWithTools eb(5);
  Serial<< F("------------------------------------") << ENDL;
  Serial << F("Testing with record of size ") << size << ENDL;
  bool result = eb.init(testKey+size, hw, size);
  if (!result) {
    Serial << F("test_EEPROM_Bank_Volume: ERROR INITIALIZING EEPROM") << ENDL;
  }
  unsigned long numLeft = eb.recordsLeftToRead();
  unsigned long numFree = eb.getNumFreeRecords();
  if (numFree == 0) {
  	  Serial << F("Error: EEPROM bank should be empty...") << ENDL;
  	  errorCount++;
  }
#ifndef TERSE_OUTPUT
  eb.printHeaderFromChip();

  Serial.println();
  Serial.print("Header key             : 0x");
  Serial.println(testKey+size, HEX);

  Serial.print("Size of record         : ");
  Serial.print(size);
  Serial.println(" bytes.");

  Serial.print("Used space             : ");
  Serial.print(numLeft);
  Serial.println(" records.");

  Serial.print("Free space             : ");
  Serial.print(numFree);
  Serial.println(" records ");

  Serial.println("----------");
  Serial << ENDL << "Writing up to " << NUM_RECORDS << " record(s) (1 dot = 100 records)" << ENDL;
#endif

 unsigned long count=0;
  while ((count < NUM_RECORDS) && eb.storeOneRecord(referenceRecord, size))
  {
    //Serial  << count << " "  << ENDL;
    //eb.printHeaderFromMemory();
	checkEEPROM_Header(eb.getHeader(), size);
    if ((count % 100) == 0) {
      Serial << ".";
      Serial.flush();
      eb.doIdle(true);
    }
    count++;
  }
  eb.doIdle(true);
  Serial << ENDL << "Wrote " << count << " records" << ENDL;


#ifdef REREAD_RECORDS
  Serial << ENDL << "Reading record(s) back..." << ENDL;
  eb.resetReader();

  numLeft = eb.recordsLeftToRead();
  for (int i = 0; i < numLeft; i ++ ) {
#ifdef DETAILED_OUTPUT
	  Serial << i << ENDL;
#endif
    bool result= eb.readOneRecord(buffer, size);
    if (!result) {
    	  Serial << F("*** Error in readOneRecord #")<< i << ENDL;
        Serial.flush();
    	  exit(-1);
    	  errorCount++;
    }
    else
    {
    // Checking
    for (byte j=0 ; j<size;j++) {
    		if (buffer[j] != j) {
    			errorCount++;
    	    Serial << "  *** ERROR: erroneous value (" << buffer[j] << ") detected (expected " << j << ") **** " << ENDL;
    	    hexDump(buffer,size);
          Serial.flush();
    	    exit(-1);
    		}
    } // for
    } // else
  } // for
#endif
  Serial << ENDL; 
#ifndef TERSE_OUTPUT
  eb.printHeaderFromChip();
#endif
  eb.resetReader();
  numLeft = eb.recordsLeftToRead();
  Serial << F("EEPROM contains ") << numLeft << " identical records." << ENDL;
  if (numLeft != count ) {
	  Serial << F("*** ERROR: inconsistent number of records") << ENDL;
	  errorCount++;
  }
}

void setup() {
  Serial.begin (19200);

  hw.init();
  hw.printFullDiagnostic(Serial);

  initReferenceRecord();
  previousHeader.firstFreeChip=99;

  // NB: do not use a byte as loop index: it would overflow if MaxRecordSize == 255.
  for (int size=MinRecordSize; size<=MaxRecordSize; size++) {
	  	testOneRecordSize((byte) size);
  }

  if (errorCount) {
	  Serial  << F("**** ") << errorCount << F(" errors detected") << ENDL;
  }
  else {
	  Serial << F("No error detected")<< ENDL;
  }
  Serial << F("End of job") << ENDL;

}

void loop() {
  // put your main code here, to run repeatedly:

}
