
#pragma once
#include "Arduino.h"

class BlinkingLED {
  public:
    BlinkingLED(byte pinNumber, unsigned long duration);
    void run();
  private:
    byte pinNumber;
    uint8_t duration;
    unsigned long ts;
} ;
